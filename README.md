# cobweb

cobweb is a greasemonkey fork with a few minor tweaks to suit me personally -- at least for now.

you can download it [here](https://gitlab.com/acaciawrites/cobweb/-/raw/master/build/cobweb-4.9-fx.xpi)
